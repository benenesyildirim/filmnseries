package com.benenesyildirim.filmsnseries.DataModel;

import java.io.Serializable;

public class ProductionCompanies implements Serializable {
    private static final long serialVersionUID = -4860359084206374197L;
    private String logo_path,name;

    public String getLogo_path() {
        return logo_path;
    }

    public void setLogo_path(String logo_path) {
        this.logo_path = logo_path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
