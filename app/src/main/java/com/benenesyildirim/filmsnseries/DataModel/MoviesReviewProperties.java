package com.benenesyildirim.filmsnseries.DataModel;

import java.io.Serializable;

public class MoviesReviewProperties implements Serializable {
    private static final long serialVersionUID = -2366791662218850979L;
    private String author, content;

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
