package com.benenesyildirim.filmsnseries.DataModel;

import java.io.Serializable;

public class GenresData implements Serializable {
    private static final long serialVersionUID = 5061400322853895747L;
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
