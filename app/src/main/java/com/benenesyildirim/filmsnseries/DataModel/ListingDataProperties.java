package com.benenesyildirim.filmsnseries.DataModel;

import java.io.Serializable;

public class ListingDataProperties implements Serializable {
    private static final long serialVersionUID = 1577082470350808358L;

    private long id;
    private String poster_path, backdrop_path;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }
}
