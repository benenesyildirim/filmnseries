package com.benenesyildirim.filmsnseries.DataModel;

public class EpisodeToAir {
    private String air_date;

    public String getAir_date() {
        return air_date;
    }

    public void setAir_date(String air_date) {
        this.air_date = air_date;
    }
}
