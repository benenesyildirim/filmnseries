package com.benenesyildirim.filmsnseries.DataModel;

import java.io.Serializable;
import java.util.List;

public class MovieReviewsData implements Serializable {
    private static final long serialVersionUID = -345668274209989763L;
    private List<MoviesReviewProperties> results;

    public List<MoviesReviewProperties> getResults() {
        return results;
    }

    public void setResults(List<MoviesReviewProperties> results) {
        this.results = results;
    }
}
