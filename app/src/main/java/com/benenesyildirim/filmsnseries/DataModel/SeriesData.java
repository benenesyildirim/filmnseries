package com.benenesyildirim.filmsnseries.DataModel;

import java.util.List;

public class SeriesData {

    private List<SeriesListingProperties> results;

    public List<SeriesListingProperties> getResults() {
        return results;
    }

    public void setResults(List<SeriesListingProperties> results) {
        this.results = results;
    }
}
