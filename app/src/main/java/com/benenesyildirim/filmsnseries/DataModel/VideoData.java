package com.benenesyildirim.filmsnseries.DataModel;

import java.util.List;

public class VideoData {
    private List<VideoProperties> results;

    public List<VideoProperties> getResults() {
        return results;
    }

    public void setResults(List<VideoProperties> results) {
        this.results = results;
    }
}
