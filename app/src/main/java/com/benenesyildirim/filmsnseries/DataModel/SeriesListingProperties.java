package com.benenesyildirim.filmsnseries.DataModel;

import java.io.Serializable;

public class SeriesListingProperties implements Serializable {
    private static final long serialVersionUID = 2895357570494719460L;

    private long id;
    private String backdrop_path,poster_path,name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getBackdrop_path() {
        return backdrop_path;
    }

    public void setBackdrop_path(String backdrop_path) {
        this.backdrop_path = backdrop_path;
    }

    public String getPoster_path() {
        return poster_path;
    }

    public void setPoster_path(String poster_path) {
        this.poster_path = poster_path;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
