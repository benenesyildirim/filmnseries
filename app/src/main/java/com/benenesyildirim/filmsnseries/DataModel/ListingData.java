package com.benenesyildirim.filmsnseries.DataModel;

import java.util.List;

public class ListingData {

    private List<ListingDataProperties> results;

    public List<ListingDataProperties> getResults() {
        return results;
    }

    public void setResults(List<ListingDataProperties> results) {
        this.results = results;
    }
}
