package com.benenesyildirim.filmsnseries

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.ImageView
import kotlin.random.Random

class SplashScreen : AppCompatActivity() {
    private var mDelayHandler: Handler? = null
    private val SPLASH_DELAY: Long = 2000
    private var imageView: ImageView? = null

    private var listOfImages = listOf(R.drawable.arrival, R.drawable.captain_marvel, R.drawable.civil_war, R.drawable.infinity_war, R.drawable.interstellar,
            R.drawable.james_bond, R.drawable.lion_king, R.drawable.logan, R.drawable.matrix, R.drawable.minions, R.drawable.mission_impossible,
            R.drawable.spiderman, R.drawable.star_wars, R.drawable.star_wars2, R.drawable.toy_story, R.drawable.wall_e, R.drawable.wreck_it_ralph)

    internal val mRunnable: Runnable = Runnable {
        if (!isFinishing) {
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        mDelayHandler = Handler()
        mDelayHandler!!.postDelayed(mRunnable, SPLASH_DELAY)

        imageView = findViewById(R.id.splash_image)

        imageView?.setImageResource(listOfImages[(0..17).random()])

        val num = Random.nextInt(0, 10)

    }

    public override fun onDestroy() {

        if (mDelayHandler != null) {
            mDelayHandler!!.removeCallbacks(mRunnable)
        }

        super.onDestroy()
    }
}