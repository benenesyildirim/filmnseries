package com.benenesyildirim.filmsnseries.CustomAdapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.benenesyildirim.filmsnseries.DataModel.ListingDataProperties;
import com.benenesyildirim.filmsnseries.R;
import com.benenesyildirim.filmsnseries.RoundedTransformation;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RvFilmsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ListingDataProperties> films;
    private OnItemClickListener onItemClickListener;

    public interface OnItemClickListener {
        void MoviesOnItemClick(ListingDataProperties movie);
    }

    public RvFilmsAdapter(List<ListingDataProperties> films, OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
        this.films = films;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int pos) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        @SuppressLint("InflateParams") View view = inflater.inflate(R.layout.poster_path_design, null);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, @SuppressLint("RecyclerView") final int i) {
        ItemHolder itemHolder = (ItemHolder) viewHolder;
        String uri = "http://image.tmdb.org/t/p/w500" + films.get(i).getPoster_path();

        if (films.get(i).getPoster_path() != null)
            Picasso.get().load(uri).transform(new RoundedTransformation(15, 0)).resize(500, 750).into(itemHolder.poster);
        else
            Picasso.get().load(R.drawable.default_poster_image).transform(new RoundedTransformation(15, 0)).resize(500, 750).into(itemHolder.poster);

        itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (onItemClickListener != null) {
                    onItemClickListener.MoviesOnItemClick(films.get(i));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return null != films ? films.size() : 0;
    }

    public class ItemHolder extends RecyclerView.ViewHolder {
        ImageView poster;

        ItemHolder(View v) {
            super(v);
            poster = v.findViewById(R.id.poster_path_image);
        }
    }
}
