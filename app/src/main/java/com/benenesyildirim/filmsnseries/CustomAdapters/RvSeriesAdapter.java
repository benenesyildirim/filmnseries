package com.benenesyildirim.filmsnseries.CustomAdapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.benenesyildirim.filmsnseries.DataModel.ListingDataProperties;
import com.benenesyildirim.filmsnseries.R;
import com.benenesyildirim.filmsnseries.RoundedTransformation;
import com.squareup.picasso.Picasso;
import java.util.List;

public class RvSeriesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<ListingDataProperties> series;
    private SeriesOnCLickListener listener;

    public interface SeriesOnCLickListener{
        void SeriesOnItemClick(ListingDataProperties tv);
    }

    public RvSeriesAdapter(List<ListingDataProperties> series, SeriesOnCLickListener listener){
        this.series = series;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = (LayoutInflater) viewGroup.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.poster_path_design,null);
        return new ItemHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int i) {
        ItemHolder itemHolder = (ItemHolder) viewHolder;
        String uri = "http://image.tmdb.org/t/p/w500" + series.get(i).getPoster_path();

        Picasso.get().load(uri).transform(new RoundedTransformation(15, 0)).resize(500,750).into(itemHolder.poster);

        itemHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null){
                    listener.SeriesOnItemClick(series.get(i));
                }
            }
        });

        if (series.get(i).getPoster_path() == null){
            Picasso.get().load(R.drawable.default_poster_image).transform(new RoundedTransformation(15, 0)).resize(500,750).into(itemHolder.poster);
        }
    }

    @Override
    public int getItemCount() {
        return  null != series ? series.size() : 0;
    }

    public class ItemHolder extends RecyclerView.ViewHolder{
        ImageView poster;

        ItemHolder(View view){
            super(view);
            poster = view.findViewById(R.id.poster_path_image);
        }
    }
}