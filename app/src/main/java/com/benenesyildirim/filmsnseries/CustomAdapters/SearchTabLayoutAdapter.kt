package com.benenesyildirim.filmsnseries.CustomAdapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.benenesyildirim.filmsnseries.Fragments.SearchPageFragment

class SearchTabLayoutAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

    override fun getCount(): Int {
        return 2
    }

    override fun getItem(position: Int): Fragment {

        when (position) {
            0 -> "Movies"
            1 -> "Series"
            else -> "Movies"
        }

        return SearchPageFragment()
    }
}