package com.benenesyildirim.filmsnseries.Fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.benenesyildirim.filmsnseries.CustomAdapters.RvFilmsAdapter;
import com.benenesyildirim.filmsnseries.CustomAdapters.RvSeriesAdapter;
import com.benenesyildirim.filmsnseries.DataModel.ListingData;
import com.benenesyildirim.filmsnseries.DataModel.ListingDataProperties;
import com.benenesyildirim.filmsnseries.DataModel.MovieDetails;
import com.benenesyildirim.filmsnseries.DataService.API;
import com.benenesyildirim.filmsnseries.DataService.RequestInterface;
import com.benenesyildirim.filmsnseries.R;
import com.benenesyildirim.filmsnseries.RoundedTransformation;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ListingPageFragment extends Fragment implements RvFilmsAdapter.OnItemClickListener, RvSeriesAdapter.SeriesOnCLickListener, View.OnClickListener {

    private View view;
    private RvFilmsAdapter filmsAdapter;
    private RvSeriesAdapter seriesAdapter;
    private RecyclerView listing1, listing2, listing3, listing4;
    private RequestInterface requestInterface;
    private TextView textView1, textView2, textView3, textView4;
    private Button homeBtn, searchBtn, favoritesBtn;
    private List<String> backdropPathList = new ArrayList<>();
    private int filledListSize;
    private MovieDetails randomMovieDetail;
    private ImageView headerImage;
    private LinearLayout loadingLL;
    private AdView bannerAdView;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup viewGroup, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.listing_page_fragment, viewGroup, false);
        filledListSize = 0;
        initViews();

        initAd();

        fillMovies();
        fillSeries();

        backPressHandler();

        getRandomMovie();
        return view;
    }

    private void initAd() {
        MobileAds.initialize(getContext(), "ca-app-pub-8661505007193806~9360851366");

        bannerAdView = view.findViewById(R.id.ad_bar_listing_fragment);
        AdRequest adRequest = new AdRequest.Builder().build();
        bannerAdView.loadAd(adRequest);
    }

    private void getRandomMovie() {
        final List<ListingDataProperties> randomMovieList = new ArrayList<>();

        requestInterface = API.getClient().create(RequestInterface.class);
        Call<ListingData> callFilms = requestInterface.getPopularFilms(String.valueOf(new Random().nextInt(50)));

        callFilms.enqueue(new Callback<ListingData>() {
            @Override
            public void onResponse(@NonNull Call<ListingData> call, @NonNull Response<ListingData> response) {
                if (response.isSuccessful()) {
                    randomMovieList.addAll(response.body().getResults());

                    ListingDataProperties tempVal = randomMovieList.get(new Random().nextInt(randomMovieList.size()));
                    Picasso.get().load("http://image.tmdb.org/t/p/w500" + tempVal.getPoster_path()).transform(new RoundedTransformation(15, 5)).into(headerImage);

                    getAndFillMovieDetails(String.valueOf(tempVal.getId()));
                }
            }

            @Override
            public void onFailure(@NonNull Call<ListingData> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "Films ERROR!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getAndFillMovieDetails(String id) {
        requestInterface = API.getClient().create(RequestInterface.class);
        Call<MovieDetails> call = requestInterface.getMovieDetails(id);

        call.enqueue(new Callback<MovieDetails>() {
            @Override
            public void onResponse(Call<MovieDetails> call, Response<MovieDetails> response) {
                if (response.isSuccessful()) {
                    randomMovieDetail = new MovieDetails();
                    randomMovieDetail = response.body();

                    fillRandomMovieDetail(randomMovieDetail);
                }
            }

            @Override
            public void onFailure(Call<MovieDetails> call, Throwable t) {
                Toast.makeText(getContext(), "Please Check Internet Connection!", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void fillRandomMovieDetail(final MovieDetails randomMovieDetail) {
        TextView randomMovieName = view.findViewById(R.id.random_movie_name);
        TextView randomMovieStatus = view.findViewById(R.id.random_movie_status);
        TextView randomMovieVote = view.findViewById(R.id.random_movie_vote);
        TextView releaseDate = view.findViewById(R.id.release_date);
        ImageButton getNewMovie = view.findViewById(R.id.new_movie_btn);
        LinearLayout randomMovieLL = view.findViewById(R.id.random_movie_ll);

        randomMovieName.setText(randomMovieDetail.getTitle());
        releaseDate.setText("Release Date: " + randomMovieDetail.getRelease_date());
        randomMovieStatus.setText("Status: " + randomMovieDetail.getStatus());
        randomMovieVote.setText("Vote: " + randomMovieDetail.getVote_average());

        getNewMovie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getRandomMovie();
            }
        });

        randomMovieLL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                ListingDataProperties movie = new ListingDataProperties();
                movie.setId(randomMovieDetail.getId());
                bundle.putSerializable("movie", movie);
                bundle.putBoolean("showType", false);
                Navigation.findNavController(view).navigate(R.id.action_listingPageFragment_to_detailPageFragment, bundle);
            }
        });
    }

    private void setAndFillHeaderImage() {
        int n = new Random().nextInt(backdropPathList.size());
//        Picasso.get().load("http://image.tmdb.org/t/p/w500" + backdropPathList.get(n)).transform(new RoundedTransformation(15, 7)).into(headerImage);
    }

    private void initViews() {
        listing1 = view.findViewById(R.id.listing1);
        listing2 = view.findViewById(R.id.listing2);
        listing3 = view.findViewById(R.id.listing3);
        listing4 = view.findViewById(R.id.listing4);

        textView1 = view.findViewById(R.id.textview1);
        textView2 = view.findViewById(R.id.textview2);
        textView3 = view.findViewById(R.id.textview3);
        textView4 = view.findViewById(R.id.textview4);

        homeBtn = view.findViewById(R.id.home_page_btn);
        searchBtn = view.findViewById(R.id.search_btn);
        favoritesBtn = view.findViewById(R.id.favorites_btn);

        headerImage = view.findViewById(R.id.header_image_listing_page);

        loadingLL = view.findViewById(R.id.loading_anim_listing_page);

        homeBtn.setOnClickListener(this);
        searchBtn.setOnClickListener(this);
        favoritesBtn.setOnClickListener(this);
    }

    //Movies
    private void fillMovies() {
        listPopularMovies();
        listUpComingMovies();
        listTopRatedMovies();
    }

    private void listPopularMovies() {
        requestInterface = API.getClient().create(RequestInterface.class);
        Call<ListingData> callFilms = requestInterface.getPopularFilms("1");

        callFilms.enqueue(new Callback<ListingData>() {
            @Override
            public void onResponse(@NonNull Call<ListingData> call, @NonNull Response<ListingData> response) {
                if (response.isSuccessful()) {
                    textView2.setText(getString(R.string.popular_movies));

                    filmsAdapter = new RvFilmsAdapter(response.body().getResults(), ListingPageFragment.this);

                    LinearLayoutManager llm = new LinearLayoutManager(getContext());
                    llm.setOrientation(LinearLayoutManager.HORIZONTAL);
                    listing2.setLayoutManager(llm);
                    listing2.setAdapter(filmsAdapter);
                    addImageBackdropPathList(response.body().getResults());


                    filledListSize++;

                    disableLoadingIfEverythingLoaded();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ListingData> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "Films ERROR!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void listUpComingMovies() {
        requestInterface = API.getClient().create(RequestInterface.class);
        Call<ListingData> callFilms = requestInterface.getUpComingFilms();

        callFilms.enqueue(new Callback<ListingData>() {
            @Override
            public void onResponse(@NonNull Call<ListingData> call, @NonNull Response<ListingData> response) {

                if (response.isSuccessful()) {
                    textView3.setText(getString(R.string.up_coming_movies));
                    filmsAdapter = new RvFilmsAdapter(response.body().getResults(), ListingPageFragment.this);

                    LinearLayoutManager llm = new LinearLayoutManager(getContext());
                    llm.setOrientation(LinearLayoutManager.HORIZONTAL);
                    listing3.setLayoutManager(llm);
                    listing3.setAdapter(filmsAdapter);

                    addImageBackdropPathList(response.body().getResults());

                    filledListSize++;

                    disableLoadingIfEverythingLoaded();

                }
            }

            @Override
            public void onFailure(@NonNull Call<ListingData> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "Films ERROR!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void listTopRatedMovies() {
        requestInterface = API.getClient().create(RequestInterface.class);
        Call<ListingData> callFilms = requestInterface.getTopRatedFilms();

        callFilms.enqueue(new Callback<ListingData>() {
            @Override
            public void onResponse(@NonNull Call<ListingData> call, @NonNull Response<ListingData> response) {
                if (response.isSuccessful()) {
                    textView4.setText(getString(R.string.top_rated_movies));
                    filmsAdapter = new RvFilmsAdapter(response.body().getResults(), ListingPageFragment.this);

                    LinearLayoutManager llm = new LinearLayoutManager(getContext());
                    llm.setOrientation(LinearLayoutManager.HORIZONTAL);
                    listing4.setLayoutManager(llm);
                    listing4.setAdapter(filmsAdapter);

                    addImageBackdropPathList(response.body().getResults());

                    filledListSize++;

                    disableLoadingIfEverythingLoaded();
                }
            }

            @Override
            public void onFailure(@NonNull Call<ListingData> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "Films ERROR!", Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void disableLoadingIfEverythingLoaded() {
        if (filledListSize == 4) {
            final Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    setAndFillHeaderImage();
                    loadingLL.setVisibility(View.GONE);
                }
            }, 1500);
        }
    }

    //Series
    private void fillSeries() {
        listPopularSeries();
    }

    private void listPopularSeries() {
        requestInterface = API.getClient().create(RequestInterface.class);
        Call<ListingData> callSeries = requestInterface.getPopularSeries();

        callSeries.enqueue(new Callback<ListingData>() {
            @Override
            public void onResponse(Call<ListingData> call, Response<ListingData> response) {
                if (response.isSuccessful()) {
                    textView1.setText(getString(R.string.popular_series));

                    seriesAdapter = new RvSeriesAdapter(response.body().getResults(), ListingPageFragment.this);

                    LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
                    linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                    listing1.setLayoutManager(linearLayoutManager);
                    listing1.setAdapter(seriesAdapter);

                    addImageBackdropPathList(response.body().getResults());

                    filledListSize++;

                    disableLoadingIfEverythingLoaded();
                }
            }

            @Override
            public void onFailure(Call<ListingData> call, Throwable t) {

            }
        });
    }

    private void addImageBackdropPathList(List<ListingDataProperties> results) {
        for (int i = 0; i < results.size(); i++)
            backdropPathList.add(results.get(i).getBackdrop_path());
    }

    @Override
    public void MoviesOnItemClick(ListingDataProperties movie) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("movie", movie);
        bundle.putBoolean("showType", false);
        Navigation.findNavController(view).navigate(R.id.action_listingPageFragment_to_detailPageFragment, bundle);
    }

    @Override
    public void SeriesOnItemClick(ListingDataProperties tv) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("tv", tv);
        bundle.putSerializable("showType", true);
        Navigation.findNavController(view).navigate(R.id.action_listingPageFragment_to_detailPageFragment, bundle);
    }

    @SuppressLint("ResourceType")
    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.search_btn:
                Navigation.findNavController(view).navigate(R.id.action_listingPageFragment_to_searchPageFragment);
                break;
            case R.id.home_page_btn:
                Navigation.findNavController(view).navigate(R.id.listingPageFragment);
                break;
            case R.id.favorites_btn:
                Navigation.findNavController(view).navigate(R.id.action_listingPageFragment_to_favoritesPageFragment);
                break;
        }

    }

    private void backPressHandler() {
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                if (!Navigation.findNavController(view).popBackStack()) {
                    new AlertDialog.Builder(getContext())
                            .setIcon(R.drawable.popcorn)
                            .setTitle("Closing Activity")
                            .setMessage("Are you sure you want to close this activity?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    getActivity().finish();
                                }

                            })
                            .setNegativeButton("No", null)
                            .show();
                }
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);
    }
}