package com.benenesyildirim.filmsnseries.Fragments;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.OnBackPressedCallback;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.benenesyildirim.filmsnseries.CustomAdapters.RvFilmsAdapter;
import com.benenesyildirim.filmsnseries.CustomAdapters.RvSeriesAdapter;
import com.benenesyildirim.filmsnseries.DataModel.MovieDetails;
import com.benenesyildirim.filmsnseries.DataModel.MovieReviewsData;
import com.benenesyildirim.filmsnseries.DataModel.ListingData;
import com.benenesyildirim.filmsnseries.DataModel.ListingDataProperties;
import com.benenesyildirim.filmsnseries.DataModel.SeriesDetails;
import com.benenesyildirim.filmsnseries.DataModel.VideoData;
import com.benenesyildirim.filmsnseries.DataService.API;
import com.benenesyildirim.filmsnseries.DataService.RequestInterface;
import com.benenesyildirim.filmsnseries.R;
import com.benenesyildirim.filmsnseries.RoundedTransformation;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.picasso.Picasso;

import java.lang.reflect.Type;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailPageFragment extends Fragment implements RvFilmsAdapter.OnItemClickListener, RvSeriesAdapter.SeriesOnCLickListener, View.OnClickListener {

    private View view;
    private ImageView posterImage, headerImage, prodCompImage1, prodCompImage2, prodCompImage3, addFavoriteImage;
    private TextView voteAverageTxt, nameTxt, taglineTxt, statusTxt, runtimeTxt, overviewTxt, genresTxt1, genresTxt2, genresTxt3,
            releaseDateTxt, revenueTxt, budgetTxt, homepageTxt, usernameTxt1, usernameTxt2, usernameTxt3, reviewTxt1, reviewTxt2,
            reviewTxt3, firstAirDateTxt, lastAirDateTxt, numberOfEpisodesTxt, nextEpisodeToAirTxt;
    private RecyclerView similarMoviesRV;
    private String id;
    private RequestInterface requestInterface;
    private LinearLayout reviewsLL, reviews1LL, reviews2LL, reviews3LL, similarMoviesLL, prodCompLL,loadingLL;
    private boolean isFavorite = false; //TODO değişiklik gerekebilir.
    private MovieDetails movieObject = new MovieDetails();
    private SeriesDetails seriesObject = new SeriesDetails();
    private boolean showType;

    private ListingDataProperties movie;
    private ListingDataProperties series;

    private List<ListingDataProperties> watchList = new ArrayList<>();

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup viewGroup, @Nullable Bundle savedInstanceState) {
        assert getArguments() != null;
        showType = getArguments().getBoolean("showType");

        if (!showType) {
            view = inflater.inflate(R.layout.movies_detail_page_fragment, viewGroup, false);
            initMovieView();
            movieDetails();
        } else {
            view = inflater.inflate(R.layout.series_detail_page_fragment, viewGroup, false);
            initSeriesView();
            seriesDetails();
        }

        backPressHandler();
        return view;
    }

    private void watchlistController() {
        SharedPreferences prefs = getContext().getSharedPreferences("watchlist", Context.MODE_PRIVATE);
        if (!showType) {

//            watchlist = prefs.getStringSet("moviesWatchlist", new HashSet<String>());
            String json = prefs.getString("moviesWatchlist", "");
            Type type = new TypeToken<List<ListingDataProperties>>() {
            }.getType();
            watchList = new Gson().fromJson(json, type);

            if (watchList != null)
                for (int i = 0; i < watchList.size(); i++)
                    if (movieObject.getId() == watchList.get(i).getId()) {
                        addFavoriteImage.setImageResource(R.drawable.favorite_star);
                        isFavorite = true;
                    }
        } else {
            String json = prefs.getString("seriesWatchlist", "");
            Type type = new TypeToken<List<ListingDataProperties>>() {
            }.getType();
            watchList = new Gson().fromJson(json, type);

            if (watchList != null)
                for (int i = 0; i < watchList.size(); i++)
                    if (seriesObject.getId() == watchList.get(i).getId()) {
                        addFavoriteImage.setImageResource(R.drawable.favorite_star);
                        isFavorite = true;
                    }
        }
    }

    private void backPressHandler() {
        OnBackPressedCallback callback = new OnBackPressedCallback(true) {
            @Override
            public void handleOnBackPressed() {
                Navigation.findNavController(view).popBackStack();
            }
        };
        requireActivity().getOnBackPressedDispatcher().addCallback(getViewLifecycleOwner(), callback);
    }

    //Movies
    private void initMovieView() {//TODO Diziler ve Filmler için özelleştirme yapmalıyım!
        posterImage = view.findViewById(R.id.poster_image);
        headerImage = view.findViewById(R.id.header_image);
        prodCompImage1 = view.findViewById(R.id.production_companies_logo1);
        prodCompLL = view.findViewById(R.id.prod_compLL);
        prodCompImage2 = view.findViewById(R.id.production_companies_logo2);
        prodCompImage3 = view.findViewById(R.id.production_companies_logo3);
        addFavoriteImage = view.findViewById(R.id.add_favorites_image);

        headerImage.setOnClickListener(this);
        addFavoriteImage.setOnClickListener(this);

        voteAverageTxt = view.findViewById(R.id.vote_average_text);
        nameTxt = view.findViewById(R.id.name_text);
        taglineTxt = view.findViewById(R.id.tagline_text);
        statusTxt = view.findViewById(R.id.status_text);
        runtimeTxt = view.findViewById(R.id.runtime_text);
        overviewTxt = view.findViewById(R.id.overview_text);
        genresTxt1 = view.findViewById(R.id.genres_text1);
        genresTxt2 = view.findViewById(R.id.genres_text2);
        genresTxt3 = view.findViewById(R.id.genres_text3);
        releaseDateTxt = view.findViewById(R.id.release_date_text);
        revenueTxt = view.findViewById(R.id.revenue_text);
        budgetTxt = view.findViewById(R.id.budget_text);
        homepageTxt = view.findViewById(R.id.homepage_text);
        usernameTxt1 = view.findViewById(R.id.username1);
        usernameTxt2 = view.findViewById(R.id.username2);
        usernameTxt3 = view.findViewById(R.id.username3);
        reviewTxt1 = view.findViewById(R.id.review1);
        reviewTxt2 = view.findViewById(R.id.review2);
        reviewTxt3 = view.findViewById(R.id.review3);

        similarMoviesRV = view.findViewById(R.id.similar_movies_RV);

        reviewsLL = view.findViewById(R.id.reviewsLL);
        reviews1LL = view.findViewById(R.id.review1LL);
        reviews2LL = view.findViewById(R.id.review2LL);
        reviews3LL = view.findViewById(R.id.review3LL);
        similarMoviesLL = view.findViewById(R.id.similarMoviesLL);

        loadingLL = view.findViewById(R.id.loading_anim_detail_page);
    }

    private void movieDetails() {
        movie = (ListingDataProperties) getArguments().getSerializable("movie");
        id = String.valueOf(movie.getId());

        getMovieDetails();
        getReviews();
        fillSimilarMovies();
    }

    private void getMovieDetails() {
        requestInterface = API.getClient().create(RequestInterface.class);
        Call<MovieDetails> call = requestInterface.getMovieDetails(id);

        call.enqueue(new Callback<MovieDetails>() {
            @Override
            public void onResponse(Call<MovieDetails> call, Response<MovieDetails> response) {
                if (response.isSuccessful()) {
                    movieObject = response.body();
                    fillMovieDetails(response);
                    watchlistController();
                }
            }

            @Override
            public void onFailure(Call<MovieDetails> call, Throwable t) {
                Toast.makeText(getContext(), "Please Check Internet Connection!", Toast.LENGTH_LONG).show();
            }
        });
    }

    public void getMovieVideo() {
        requestInterface = API.getClient().create(RequestInterface.class);
        Call<VideoData> call = requestInterface.getMovieVideos(id);

        call.enqueue(new Callback<VideoData>() {
            @Override
            public void onResponse(Call<VideoData> call, Response<VideoData> response) {
                if (!response.body().getResults().isEmpty()) {
                    String url = "https://www.youtube.com/watch?v=" + response.body().getResults().get(0).getKey();
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<VideoData> call, Throwable t) {
                Toast.makeText(getContext(), "Can't Find Video!", Toast.LENGTH_LONG).show();
            }
        });
    }

    private void fillMovieDetails(Response<MovieDetails> response) {
        Picasso.get().load("http://image.tmdb.org/t/p/w500" + response.body().getBackdrop_path()).into(headerImage);
        Picasso.get().load("http://image.tmdb.org/t/p/w500" + response.body().getPoster_path()).transform(new RoundedTransformation(15, 0)).into(posterImage);
        nameTxt.setText(response.body().getTitle());
        taglineTxt.setText(response.body().getTagline());
        statusTxt.setText(response.body().getStatus());
        if (response.body().getRuntime() != 0)
            runtimeTxt.setText(convertTime(response.body().getRuntime()));
        else
            runtimeTxt.setText("Unknown");
        overviewTxt.setText(response.body().getOverview());
        voteAverageTxt.setText(response.body().getVote_average());

        for (int i = 0; i < response.body().getGenres().size(); i++) {
            if (i == 3)
                break;
            if (i == 0) {
                genresTxt1.setText(response.body().getGenres().get(0).getName());
                genresTxt1.setVisibility(View.VISIBLE);
            }
            if (i == 1) {
                genresTxt2.setText(response.body().getGenres().get(1).getName());
                genresTxt2.setVisibility(View.VISIBLE);
            }
            if (i == 2) {
                genresTxt3.setText(response.body().getGenres().get(2).getName());
                genresTxt3.setVisibility(View.VISIBLE);
            }
        }

        releaseDateTxt.setText(response.body().getRelease_date());

        if (response.body().getRevenue() != 0) {
            revenueTxt.setText(moneyFormatter(response.body().getRevenue()) + " $");
        } else
            revenueTxt.setText("Unknown");

        if (response.body().getBudget() != 0)
            budgetTxt.setText(moneyFormatter(response.body().getBudget()) + " $");
        else
            budgetTxt.setText("Unknown");

        homepageTxt.setText(response.body().getHomepage()); //TODO Link verilecek!

        int genresLogoCount = 3;

        for (int i = 0; i < response.body().getProduction_companies().size(); i++) {
            if (i == genresLogoCount)
                break;
            if (i == 0) {
                if (response.body().getProduction_companies().get(i).getLogo_path() != null) {
                    Picasso.get().load("http://image.tmdb.org/t/p/w500" + response.body().getProduction_companies().get(i).getLogo_path()).into(prodCompImage1);
                    prodCompImage1.setVisibility(View.VISIBLE);
                    prodCompLL.setVisibility(View.VISIBLE);
                }
            }
            if (i == 1) {
                if (response.body().getProduction_companies().get(i).getLogo_path() != null) {
                    {
                        Picasso.get().load("http://image.tmdb.org/t/p/w500" + response.body().getProduction_companies().get(i).getLogo_path()).into(prodCompImage2);
                        prodCompImage2.setVisibility(View.VISIBLE);
                        prodCompLL.setVisibility(View.VISIBLE);
                    }
                }
            }
            if (i == 2) {
                if (response.body().getProduction_companies().get(i).getLogo_path() != null) {
                    {
                        Picasso.get().load("http://image.tmdb.org/t/p/w500" + response.body().getProduction_companies().get(i).getLogo_path()).into(prodCompImage3);
                        prodCompImage3.setVisibility(View.VISIBLE);
                        prodCompLL.setVisibility(View.VISIBLE);
                    }
                }
            }
        }
    }

    private void fillSimilarMovies() {
        requestInterface = API.getClient().create(RequestInterface.class);
        Call<ListingData> callSimilarMovies = requestInterface.getSimilarMovies(id);

        callSimilarMovies.enqueue(new Callback<ListingData>() {
            @Override
            public void onResponse(Call<ListingData> call, Response<ListingData> response) {
                if (response.isSuccessful()) {
                    if (response.body().getResults().size() != 0)
                        similarMoviesLL.setVisibility(View.VISIBLE);
                    RvFilmsAdapter filmsAdapter = new RvFilmsAdapter(response.body().getResults(), DetailPageFragment.this);
                    LinearLayoutManager llm = new LinearLayoutManager(getContext());
                    llm.setOrientation(LinearLayoutManager.HORIZONTAL);
                    similarMoviesRV.setLayoutManager(llm);
                    similarMoviesRV.setAdapter(filmsAdapter);

                    loadingLL.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ListingData> call, Throwable t) {

            }
        });
    }

    @Override
    public void MoviesOnItemClick(ListingDataProperties movie) {
        Bundle bundle = new Bundle();
        bundle.putSerializable("movie", movie);
        bundle.putBoolean("showType", false);
        Navigation.findNavController(view).navigate(R.id.detailPageFragment, bundle);
    }

    //Series
    private void initSeriesView() {
        posterImage = view.findViewById(R.id.poster_image);
        headerImage = view.findViewById(R.id.header_image);
        prodCompLL = view.findViewById(R.id.prod_compLL);
        prodCompImage1 = view.findViewById(R.id.production_companies_logo1);
        prodCompImage2 = view.findViewById(R.id.production_companies_logo2);
        prodCompImage3 = view.findViewById(R.id.production_companies_logo3);
        addFavoriteImage = view.findViewById(R.id.add_favorites_image);

        addFavoriteImage.setOnClickListener(this);

        voteAverageTxt = view.findViewById(R.id.vote_average_text);
        nameTxt = view.findViewById(R.id.name_text);
        taglineTxt = view.findViewById(R.id.tagline_text);
        statusTxt = view.findViewById(R.id.status_text);
        firstAirDateTxt = view.findViewById(R.id.first_air_date_text);//TODO FAD
        overviewTxt = view.findViewById(R.id.overview_text);
        genresTxt1 = view.findViewById(R.id.genres_text1);
        genresTxt2 = view.findViewById(R.id.genres_text2);
        genresTxt3 = view.findViewById(R.id.genres_text3);
        lastAirDateTxt = view.findViewById(R.id.last_air_date_text);//TODO LAD
        numberOfEpisodesTxt = view.findViewById(R.id.number_of_episodes_text);//TODO NOE
        nextEpisodeToAirTxt = view.findViewById(R.id.next_episode_to_air);//TODO NEA
        homepageTxt = view.findViewById(R.id.homepage_text);
        usernameTxt1 = view.findViewById(R.id.username1);
        usernameTxt2 = view.findViewById(R.id.username2);
        usernameTxt3 = view.findViewById(R.id.username3);
        reviewTxt1 = view.findViewById(R.id.review1);
        reviewTxt2 = view.findViewById(R.id.review2);
        reviewTxt3 = view.findViewById(R.id.review3);

        similarMoviesRV = view.findViewById(R.id.similar_movies_RV);

        reviewsLL = view.findViewById(R.id.reviewsLL);
        reviews1LL = view.findViewById(R.id.review1LL);
        reviews2LL = view.findViewById(R.id.review2LL);
        reviews3LL = view.findViewById(R.id.review3LL);
        similarMoviesLL = view.findViewById(R.id.similarMoviesLL);

        loadingLL = view.findViewById(R.id.loading_anim_detail_page);

        posterImage.setOnClickListener(this);
    }

    private void seriesDetails() {
        series = (ListingDataProperties) getArguments().getSerializable("tv");
        id = String.valueOf(series.getId());

        getSeriesDetails();
        getReviews();
        getSimilarSeries();
    }

    private void getSeriesDetails() {
        requestInterface = API.getClient().create(RequestInterface.class);
        Call<SeriesDetails> call = requestInterface.getSeriesDetail(id);

        call.enqueue(new Callback<SeriesDetails>() {
            @Override
            public void onResponse(Call<SeriesDetails> call, Response<SeriesDetails> response) {
                fillSeriesDetails(response);
                seriesObject = response.body();
                watchlistController();
            }

            @Override
            public void onFailure(Call<SeriesDetails> call, Throwable t) {
                Toast.makeText(getContext(), "Please Check Internet Connection!", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void fillSeriesDetails(Response<SeriesDetails> response) {
        Picasso.get().load("http://image.tmdb.org/t/p/w500" + response.body().getBackdrop_path()).into(headerImage);
        Picasso.get().load("http://image.tmdb.org/t/p/w500" + response.body().getPoster_path()).transform(new RoundedTransformation(15, 0)).into(posterImage);
        nameTxt.setText(response.body().getName());
        statusTxt.setText(response.body().getStatus());
        firstAirDateTxt.setText(response.body().getFirst_air_date());
        overviewTxt.setText(response.body().getOverview());
        voteAverageTxt.setText(String.valueOf(response.body().getVote_average()));
        lastAirDateTxt.setText(response.body().getLast_air_date());
        numberOfEpisodesTxt.setText(String.valueOf(response.body().getNumber_of_seasons() + "/" + response.body().getNumber_of_episodes()));

        for (int i = 0; i < response.body().getGenres().size(); i++) {
            if (i == 3)
                break;
            if (i == 0) {
                genresTxt1.setText(response.body().getGenres().get(0).getName());
                genresTxt1.setVisibility(View.VISIBLE);
            }
            if (i == 1) {
                genresTxt2.setText(response.body().getGenres().get(1).getName());
                genresTxt2.setVisibility(View.VISIBLE);
            }
            if (i == 2) {
                genresTxt3.setText(response.body().getGenres().get(2).getName());
                genresTxt3.setVisibility(View.VISIBLE);
            }
        }

        if (response.body().getNext_episode_to_air() != null)
            nextEpisodeToAirTxt.setText(response.body().getNext_episode_to_air().getAir_date());///TODO Null geliyor bazen!

        homepageTxt.setText(response.body().getHomepage()); //TODO Link verilecek!

        int genresLogoCount = 3;

        for (int i = 0; i < response.body().getProduction_companies().size(); i++) {
            if (i == genresLogoCount)
                break;
            if (i == 0) {
                if (response.body().getProduction_companies().get(i).getLogo_path() != null) {
                    Picasso.get().load("http://image.tmdb.org/t/p/w500" + response.body().getProduction_companies().get(i).getLogo_path()).into(prodCompImage1);
                    prodCompImage1.setVisibility(View.VISIBLE);
                    prodCompLL.setVisibility(View.VISIBLE);
                }
            }
            if (i == 1) {
                if (response.body().getProduction_companies().get(i).getLogo_path() != null) {
                    Picasso.get().load("http://image.tmdb.org/t/p/w500" + response.body().getProduction_companies().get(i).getLogo_path()).into(prodCompImage2);
                    prodCompImage2.setVisibility(View.VISIBLE);
                    prodCompLL.setVisibility(View.VISIBLE);
                }
            }
            if (i == 2) {
                if (response.body().getProduction_companies().get(i).getLogo_path() != null) {
                    Picasso.get().load("http://image.tmdb.org/t/p/w500" + response.body().getProduction_companies().get(i).getLogo_path()).into(prodCompImage3);
                    prodCompImage3.setVisibility(View.VISIBLE);
                    prodCompLL.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void getSimilarSeries() {
        requestInterface = API.getClient().create(RequestInterface.class);
        Call<ListingData> call = requestInterface.getSimilarSeries(id);

        call.enqueue(new Callback<ListingData>() {
            @Override
            public void onResponse(Call<ListingData> call, Response<ListingData> response) {
                if (response.isSuccessful()) {
                    if (response.body().getResults().size() != 0)
                        similarMoviesLL.setVisibility(View.VISIBLE);
                    RvSeriesAdapter seriesAdapter = new RvSeriesAdapter(response.body().getResults(), DetailPageFragment.this);
                    LinearLayoutManager llm = new LinearLayoutManager(getContext());
                    llm.setOrientation(LinearLayoutManager.HORIZONTAL);
                    similarMoviesRV.setLayoutManager(llm);
                    similarMoviesRV.setAdapter(seriesAdapter);

                    loadingLL.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<ListingData> call, Throwable t) {

            }
        });
    }

    @Override
    public void SeriesOnItemClick(ListingDataProperties tv) {
        id = String.valueOf(tv.getId());
        getSeriesDetails();
        getReviews();
        getSimilarSeries();
    }

    //Others
    private void getReviews() {
        requestInterface = API.getClient().create(RequestInterface.class);
        Call<MovieReviewsData> call = requestInterface.getMovieReviews(id);

        call.enqueue(new Callback<MovieReviewsData>() {
            @Override
            public void onResponse(Call<MovieReviewsData> call, Response<MovieReviewsData> response) {
                if (response.isSuccessful()) {
                    for (int i = 0; i < response.body().getResults().size(); i++) {
                        if (i == 0) {
                            reviewsLL.setVisibility(View.VISIBLE);
                            reviews1LL.setVisibility(View.VISIBLE);
                            usernameTxt1.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_logo, 0, 0, 0);
                            usernameTxt1.setText(" " + response.body().getResults().get(i).getAuthor());
                            reviewTxt1.setText(response.body().getResults().get(i).getContent());
                        } else if (i == 1) {
                            reviews2LL.setVisibility(View.VISIBLE);
                            usernameTxt2.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_logo, 0, 0, 0);
                            usernameTxt2.setText(" " + response.body().getResults().get(i).getAuthor());
                            reviewTxt2.setText(response.body().getResults().get(i).getContent());
                        } else if (i == 2) {
                            reviews3LL.setVisibility(View.VISIBLE);
                            usernameTxt3.setCompoundDrawablesWithIntrinsicBounds(R.drawable.user_logo, 0, 0, 0);
                            usernameTxt3.setText(" " + response.body().getResults().get(i).getAuthor());
                            reviewTxt3.setText(response.body().getResults().get(i).getContent());
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<MovieReviewsData> call, Throwable t) {
                reviewsLL.setVisibility(View.GONE);
            }
        });
    }

    private String convertTime(int runtime) {
        return String.format("%02d:%02d", runtime / 60, runtime % 60) + " Hours";
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == headerImage.getId()) {
            Toast.makeText(getContext(), getString(R.string.forwarding), Toast.LENGTH_SHORT).show();
            getMovieVideo();
        }
        if (v.getId() == addFavoriteImage.getId()) {
            if (isFavorite) {
                isFavorite = false;
                addFavoriteImage.setImageResource(R.drawable.non_favorite_star);
                if (!showType) {
                    for (int i = 0; i < watchList.size(); i++)
                        if (movie.getId() == watchList.get(i).getId())
                            watchList.remove(i);
                }else if (showType)
                    for (int y = 0; y < watchList.size(); y++)
                        if (series.getId() == watchList.get(y).getId())
                            watchList.remove(y);


                saveWatchlistToPreferences();
            } else {
                addFavoriteImage.setImageResource(R.drawable.favorite_star);
                isFavorite = true;

                if (!showType) {
                    if (watchList != null)
                        watchList.add(movie);
                    else {
                        watchList = new ArrayList<>();
                        watchList.add(movie);
                    }
                } else if (showType) {
                    if (watchList != null)
                        watchList.add(series);
                    else {
                        watchList = new ArrayList<>();
                        watchList.add(series);
                    }
                }

                saveWatchlistToPreferences();
            }
        }
    }

    private void saveWatchlistToPreferences() {
        SharedPreferences prefs = requireContext().getSharedPreferences("watchlist", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();

        Gson gson = new Gson();
        String watchListString = gson.toJson(watchList);

        if (!showType)
            editor.putString("moviesWatchlist", watchListString).apply();
        else
            editor.putString("seriesWatchlist", watchListString).apply();
    }

    public String moneyFormatter(double number) {
        NumberFormat formatter = new DecimalFormat("#,###");
        return formatter.format(number);
    }
}