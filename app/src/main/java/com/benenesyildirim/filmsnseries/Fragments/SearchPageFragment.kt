package com.benenesyildirim.filmsnseries.Fragments

import android.annotation.SuppressLint
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.benenesyildirim.filmsnseries.CustomAdapters.RvFilmsAdapter
import com.benenesyildirim.filmsnseries.CustomAdapters.RvSeriesAdapter
import com.benenesyildirim.filmsnseries.DataModel.ListingData
import com.benenesyildirim.filmsnseries.DataModel.ListingDataProperties
import com.benenesyildirim.filmsnseries.DataService.API
import com.benenesyildirim.filmsnseries.DataService.RequestInterface
import com.benenesyildirim.filmsnseries.R
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchPageFragment : Fragment(), View.OnClickListener, RvFilmsAdapter.OnItemClickListener, RvSeriesAdapter.SeriesOnCLickListener {

    private var searchBar: EditText? = null
    private var recyclerList: RecyclerView? = null
    private var moviesButton: Button? = null
    private var seriesButton: Button? = null
    private var moviesOrSeriesBoolean: Boolean? = true
    private var searchString: String? = null
    private var seriesSelectedView: View? = null
    private var moviesSelectedView: View? = null
    private var bannerAdView : AdView? = null

    private var loadingLL: LinearLayout? = null


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.search_page_fragment, container, false)
        initView(view)
        initAd()

        searchBar?.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                loadingLL?.visibility = View.VISIBLE
                Toast.makeText(context, "Movie is $s", Toast.LENGTH_SHORT).show()
                if (s != "")
                    getMoviesOrSeries(moviesOrSeriesBoolean, s.toString())
                else
                    loadingLL?.visibility = View.GONE

                searchString = s.toString()
            }
        })

        backPressHandler()
        return view
    }

    @SuppressLint("NewApi")
    private fun initView(view: View?) {
        searchBar = view?.findViewById(R.id.search_bar)
        recyclerList = view?.findViewById(R.id.search_recycler_view)
        moviesButton = view?.findViewById(R.id.movies_button)
        seriesButton = view?.findViewById(R.id.series_button)

        moviesSelectedView = view?.findViewById(R.id.movies_selected_view)
        seriesSelectedView = view?.findViewById(R.id.series_selected_view)

        loadingLL = view?.findViewById(R.id.loading_anim_search_page)

        bannerAdView = view?.findViewById(R.id.ad_bar_search_fragment)

        moviesButton?.setOnClickListener(this)
        seriesButton?.setOnClickListener(this)
    }

    private fun getMoviesOrSeries(result: Boolean?, query: String) {
        when (result) {
            true -> searchForMovies(query)
            false -> searchForSeries(query)
        }
    }

    private fun searchForMovies(query: String) {
        recyclerList?.layoutManager = GridLayoutManager(context, 2)
        API.getClient().create(RequestInterface::class.java)
                .getMovieSearchResults(query).enqueue(object : Callback<ListingData> {
                    override fun onFailure(call: Call<ListingData>, t: Throwable) {
                        Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<ListingData>, response: Response<ListingData>) {
                        recyclerList?.adapter = RvFilmsAdapter(response.body()?.results, this@SearchPageFragment)
                        loadingLL?.visibility = View.GONE
                    }
                })
    }

    private fun searchForSeries(query: String) {
        recyclerList?.layoutManager = GridLayoutManager(context, 2)
        API.getClient().create(RequestInterface::class.java)
                .getSeriesSearchResults(query).enqueue(object : Callback<ListingData> {
                    override fun onFailure(call: Call<ListingData>, t: Throwable) {
                        Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(call: Call<ListingData>, response: Response<ListingData>) {
                        recyclerList?.adapter = RvSeriesAdapter(response.body()?.results, this@SearchPageFragment)
                        loadingLL?.visibility = View.GONE
                    }
                })
    }

    @SuppressLint("ShowToast")
    override fun onClick(v: View?) {
        //TODO Hide Keyboard Automatically
        when (v?.id) {
            moviesButton?.id -> {
                moviesOrSeriesBoolean = true
                if (!searchString.isNullOrEmpty())
                    getMoviesOrSeries(moviesOrSeriesBoolean, searchString!!)

                seriesSelectedView?.visibility = View.GONE
                moviesSelectedView?.visibility = View.VISIBLE
            }
            seriesButton?.id -> {
                moviesOrSeriesBoolean = false
                if (!searchString.isNullOrEmpty())
                    getMoviesOrSeries(moviesOrSeriesBoolean, searchString!!)

                seriesSelectedView?.visibility = View.VISIBLE
                moviesSelectedView?.visibility = View.GONE
            }
            else -> Toast.makeText(context, "Error", Toast.LENGTH_SHORT).show()
        }
    }

    @SuppressLint("CommitTransaction")
    override fun MoviesOnItemClick(movie: ListingDataProperties?) {
        val detailPageFragment = DetailPageFragment()
        val bundle = Bundle()
        bundle.putSerializable("movie", movie)
        bundle.putBoolean("showType", false)
        detailPageFragment.arguments = bundle

        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.main_fragment, detailPageFragment, "DetailPageFragment")
        transaction?.addToBackStack("DetailPageFragment")
        transaction?.commit()
    }

    override fun SeriesOnItemClick(tv: ListingDataProperties?) {
        val detailPageFragment = DetailPageFragment()
        val bundle = Bundle()
        bundle.putSerializable("tv", tv)
        bundle.putBoolean("showType", true)
        detailPageFragment.arguments = bundle

        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.main_fragment, detailPageFragment, "DetailPageFragment")
        transaction?.addToBackStack("DetailPageFragment")
        transaction?.commit()
    }

    private fun initAd() {
        MobileAds.initialize(context, "ca-app-pub-8661505007193806~9360851366")
        val adRequest = AdRequest.Builder().build()
        bannerAdView?.loadAd(adRequest)
    }

    private fun backPressHandler() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Navigation.findNavController(view!!).popBackStack()
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }
}
