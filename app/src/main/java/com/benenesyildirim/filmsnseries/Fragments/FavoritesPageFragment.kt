package com.benenesyildirim.filmsnseries.Fragments

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.Toast
import androidx.activity.OnBackPressedCallback
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.benenesyildirim.filmsnseries.CustomAdapters.RvFilmsAdapter
import com.benenesyildirim.filmsnseries.CustomAdapters.RvSeriesAdapter
import com.benenesyildirim.filmsnseries.DataModel.ListingDataProperties
import com.benenesyildirim.filmsnseries.R
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.MobileAds
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


class FavoritesPageFragment : Fragment(), RvFilmsAdapter.OnItemClickListener, RvSeriesAdapter.SeriesOnCLickListener {
    private var moviesFavoritesList: RecyclerView? = null
    private var seriesFavoritesList: RecyclerView? = null
    private var moviesWatchList: List<ListingDataProperties> = mutableListOf()
    private var seriesWatchList: List<ListingDataProperties> = mutableListOf()
    private var loadingLL: LinearLayout? = null

    private var bannerAdView: AdView? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.favorites_page_fragment, container, false)

        initView(view)
        initAd()

        getWatchlist()

        fillMoviesList()
        fillSeriesList()

        loadingLL?.visibility = View.GONE

        backPressHandler()
        return view
    }

    private fun getWatchlist() {
        val prefences = context?.getSharedPreferences("watchlist", Context.MODE_PRIVATE)
        val moviesWatchListString = prefences?.getString("moviesWatchlist", null)

        val type: Type = object : TypeToken<List<ListingDataProperties?>?>() {}.type
        val gson = Gson()
        if (moviesWatchListString != null)
            moviesWatchList = gson.fromJson(moviesWatchListString, type)

        val seriesWatchListString = prefences?.getString("seriesWatchlist", null)

        if (seriesWatchListString != null)
            seriesWatchList = gson.fromJson(seriesWatchListString, type)

        if(seriesWatchListString == null && moviesWatchListString == null)
            Toast.makeText(context,"There is no movies or series...",Toast.LENGTH_SHORT).show()
    }

    private fun initView(v: View) {
        moviesFavoritesList = v.findViewById(R.id.movies_favorites_list)
        seriesFavoritesList = v.findViewById(R.id.series_favorites_list)

        bannerAdView = v.findViewById(R.id.ad_bar_favorites_fragment)

        loadingLL = v.findViewById(R.id.loading_anim_favorites_page)
    }

    @SuppressLint("WrongConstant")
    private fun fillMoviesList() {
        moviesFavoritesList?.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        moviesFavoritesList?.adapter = RvFilmsAdapter(moviesWatchList, this)
    }

    @SuppressLint("WrongConstant")
    private fun fillSeriesList() {
        seriesFavoritesList?.layoutManager = LinearLayoutManager(context, LinearLayout.VERTICAL, false)
        seriesFavoritesList?.adapter = RvSeriesAdapter(seriesWatchList, this)
    }

    override fun MoviesOnItemClick(movie: ListingDataProperties?) {
        val bundle = Bundle()
        bundle.putSerializable("movie", movie)
        bundle.putBoolean("showType", false)
        Navigation.findNavController(requireView()).navigate(R.id.detailPageFragment, bundle)
    }

    override fun SeriesOnItemClick(tv: ListingDataProperties?) {
        val bundle = Bundle()
        bundle.putSerializable("tv", tv)
        bundle.putSerializable("showType", true)
        Navigation.findNavController(requireView()).navigate(R.id.detailPageFragment, bundle)
    }

    private fun initAd() {
        MobileAds.initialize(context, "ca-app-pub-8661505007193806~9360851366")
        val adRequest = AdRequest.Builder().build()
        bannerAdView?.loadAd(adRequest)
    }

    private fun backPressHandler() {
        val callback: OnBackPressedCallback = object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                Navigation.findNavController(view!!).popBackStack()// TODO Bunu sil burtaya yapıştır.
            }
        }
        requireActivity().onBackPressedDispatcher.addCallback(viewLifecycleOwner, callback)
    }
}