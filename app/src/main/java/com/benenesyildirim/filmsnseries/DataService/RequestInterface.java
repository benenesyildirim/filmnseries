package com.benenesyildirim.filmsnseries.DataService;

import com.benenesyildirim.filmsnseries.DataModel.MovieDetails;
import com.benenesyildirim.filmsnseries.DataModel.MovieReviewsData;
import com.benenesyildirim.filmsnseries.DataModel.ListingData;
import com.benenesyildirim.filmsnseries.DataModel.SeriesDetails;
import com.benenesyildirim.filmsnseries.DataModel.VideoData;
import com.benenesyildirim.filmsnseries.DataModel.SeriesData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface RequestInterface {

    //FILMS
    @GET("3/movie/popular?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US")
    Call<ListingData> getPopularFilms(@Query("page") String value);

    @GET("3/movie/upcoming?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US&page=1")
    Call<ListingData> getUpComingFilms();

    @GET("3/movie/now_playing?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US&page=1")
    Call<ListingData> getNowPlayingFilms();

    @GET("3/movie/top_rated?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US&page=1")
    Call<ListingData> getTopRatedFilms();

    @GET("3/movie/{ID}?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US")
    Call<MovieDetails> getMovieDetails(@Path("ID") String movieID);

    @GET("3/movie/{ID}/videos?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US")
    Call<VideoData> getMovieVideos(@Path("ID") String movieID);

    @GET("3/movie/{ID}/similar?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US&page=1")
    Call<ListingData> getSimilarMovies(@Path("ID") String movieID);

    @GET("3/movie/{ID}/reviews?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US&page=1")
    Call<MovieReviewsData> getMovieReviews(@Path("ID") String movieID);

    @GET("3/search/movie?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US&page=1&include_adult=false")
    Call<ListingData> getMovieSearchResults(@Query("query") String searchQuery);

    //SERIES
    @GET("3/tv/popular?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US&page=1")
    Call<ListingData> getPopularSeries();

    @GET("3/tv/top_rated?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US&page=1")
    Call<SeriesData> getTopRatedSeries();

    @GET("3/tv/airing_today?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US&page=1")
    Call<SeriesData> getAiringTodaySeries();

    @GET("3/tv/on_the_air?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US&page=1")
    Call<SeriesData> getTVOnTheAirSeries();

    @GET("3/tv/{ID}?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US")
    Call<SeriesDetails> getSeriesDetail(@Path("ID") String SeriesID);

    @GET("3/tv/{ID}/similar?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US&page=1")
    Call<ListingData> getSimilarSeries(@Path("ID") String SeriesID);

    @GET("3/tv/{ID}/videos?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US")
    Call<VideoData> getSeriesVideos(@Path("ID") String SeriesID);

    @GET("3/search/tv?api_key=1282b76bc545a3a1704e15ab0ddf8352&language=en-US&page=1&include_adult=false")
    Call<ListingData> getSeriesSearchResults(@Query("query") String searchQuery);

    //All
    @GET("3/trending/all/week?api_key=1282b76bc545a3a1704e15ab0ddf8352")
    Call<SeriesData> getTrendings();
}
